import React from "react";
import { useTranslation } from "react-i18next";
import { Select, Box, Label, Flex } from "vactory-ui";

const PostsFormFilter = ({
  terms,
  cities,
  value,
  handleChangeCategory,
  handleChangeCity,
}) => {
  const { t } = useTranslation();
  return (
    <Box pt="10px" pb="30px" px="xsmall">
      <Flex flexDirection={['column', 'row']}>
        <Flex flexDirection="column" mx="xsmall" mb={['10px', '0px']}>
          <Label htmlFor="events-category" mb="xsmall">
            {t("Thématique")}
          </Label>
          <Select
            id="events-category"
            onBlur={null}
            onChange={(e) => handleChangeCategory(e.currentTarget.value)}
            defaultValue={value}
          >
            <option value="all">{t("Tous les thématiques")}</option>
            {terms.map((term) => {
              return (
                <option key={term.id} value={term.id}>
                  {term.label}
                </option>
              );
            })}
          </Select>
        </Flex>

        <Flex flexDirection="column" mx="xsmall">
          <Label htmlFor="events-cities" mb="xsmall">
            {t("Ville")}
          </Label>
          <Select
            id="events-cities"
            onBlur={null}
            onChange={(e) => handleChangeCity(e.currentTarget.value)}
            defaultValue={value}
          >
            <option value="all">{t("Toutes les villes")}</option>
            {cities.map((term) => {
              return (
                <option key={term.id} value={term.id}>
                  {term.label}
                </option>
              );
            })}
          </Select>
        </Flex>
      </Flex>
    </Box>
  );
};

export default PostsFormFilter;
